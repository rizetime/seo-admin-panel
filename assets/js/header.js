/*   ---------------------- Constants ----------------------   */
var userLogout = document.querySelector('#user-logout');
var reminder = document.querySelector('#reminder img');
/*   ----------------------**** Constants ****----------------------   */
/*   ---------------------- Functions ----------------------   */
// Open/Close Logout menu
function openUserLogoutMenu() {
    var _this = this;
    _this.classList.toggle('is-open');
}
// Open/Close Reminder
function openReminder() {
    var _this = this;
    var reminder = _this.parentNode;
    reminder.classList.toggle('is-open');
}
/*   ----------------------**** Functions ****----------------------   */
/*   ---------------------- Listeners ----------------------   */
// Open/Close User-logout menu
userLogout.addEventListener('click', openUserLogoutMenu);
// Open/Close Reminder
reminder.addEventListener('click', openReminder);
/*   ----------------------**** Listeners ****----------------------   */
