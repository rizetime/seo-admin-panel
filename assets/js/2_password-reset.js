/*   ---------------------- Constants ----------------------   */
var passResetEmail = document.querySelector('#pass-reset-email');
var passResetSubmit = document.querySelector('#pass-reset-submit');
var loader = document.querySelector('.loader-circle');
/*   ----------------------**** Constants ****----------------------   */
/*   ---------------------- Functions ----------------------   */
/* Remove Alert */
function removeAlert() {
    var _this = this;
    passResetSubmit.classList.remove('invalid');
    _this.classList.remove('invalid');
}
/* Pattern for E-mail validation */
function validateEmailPattern(email) {
    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email);
}
/* Request emulating */
function startRequest() {
    setTimeout(function () {
        passResetEmail.value = '';
        passResetEmail.classList.remove('confirmed');
        passResetSubmit.removeAttribute('disabled');
        loader.classList.remove('active');
    }, 500);
}
/* Start email validation emulating */
function emailValidation() {
    var value = passResetEmail.value;
    if (validateEmailPattern(value)) {
        // if valid E-mail
        passResetEmail.classList.add('confirmed');
        loader.classList.add('active');
        passResetSubmit.setAttribute('disabled', 'disabled');
        startRequest();
    }
    else {
        // if E-mail not valid
        passResetEmail.classList.add('invalid');
        passResetSubmit.classList.add('invalid');
    }
}
/*   ----------------------**** Functions ****----------------------   */
/*   ---------------------- Listeners ----------------------   */
// Remove alerts
passResetEmail.addEventListener('input', removeAlert);
// Start email validation on submit
passResetSubmit.addEventListener('click', emailValidation);
/*   ----------------------**** Listeners ****----------------------   */
