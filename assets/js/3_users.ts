/*   ---------------------- Constants ----------------------   */
const dropDownListHead                    = document.querySelector('.dropdown-list .dropdown-list__head'),
      dropMenuItems                       = document.querySelectorAll('.drop-menu .drop-menu__item'),
      usersEmail:HTMLInputElement         = document.querySelector('#user-email'),
      usersSubmit:HTMLButtonElement       = document.querySelector('#user-submit'),
      usersTable                          = document.querySelector('#users-list-table'),
      loader                              = document.querySelector('.loader-circle'),
      popUpWindow                         = document.querySelector('#pop-up-window'),
      popUpBlur                           = document.querySelector('#pop-up-blur');
/*   ----------------------**** Constants ****----------------------   */


/*   ---------------------- Functions ----------------------   */

/* Remove Alert */
function removeAlert():void {
    const _this = this;
          _this.classList.remove('invalid');
          usersSubmit.classList.remove('invalid');
}

/* Pattern for E-mail validation */
function validateEmailPattern( email:string ):boolean {
    const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email);
}

/* Request emulating */
function startRequest():void {

    setTimeout(()=>{
        usersEmail.classList.remove('confirmed');
        usersSubmit.removeAttribute('disabled');
        loader.classList.remove('active');
        usersTable.prepend(createdItem());
        usersEmail.value = '';
        removeListItem();
        editItem();
        chooseEditStatus();
    },500);

}

/* Start email validation emulating */
function emailValidation() {
    const value = usersEmail.value;

    if ( validateEmailPattern(value) ) {
        // if Valid E-mail
        usersEmail.classList.add('confirmed');
        usersSubmit.setAttribute('disabled', 'disabled');
        loader.classList.add('active');
        startRequest();
    } else {
        // if E-mail not valid
        usersEmail.classList.add('invalid');
        usersSubmit.classList.add('invalid');
    }
}

/* Show (Open/Close) Dropdown list */
function showDropDownList() {
    const _this = this;
    _this.parentNode.classList.toggle('is-open');
}

/* Choose Item in Dropdown List*/
function chooseDropDownList() {
    const _this         = this,
        projectValue:HTMLInputElement  = document.querySelector('#users-project-value'),
        currentValue  = _this.textContent,
        dropMenu      = _this.parentNode,
        body          = dropMenu.parentNode,
        head          = body.previousElementSibling,
        dropDownList  = head.parentNode,
        headTitle     = head.querySelector('.dropdown-title .dropdown-title__value');

    dropDownList.classList.remove('is-open');
    headTitle.innerText = currentValue;
    projectValue.value = currentValue;
}

/* Create new List item */
function createdItem():HTMLLIElement {
    const project:HTMLInputElement  = document.querySelector('#users-project-value'),
          projectValue              = project.value,
          emailValue                = usersEmail.value,
          li                        = document.createElement('li');
          li.classList.add('table-body__item');
          li.innerHTML = `<a href="#" class="email">${emailValue}</a>
                                       <div class="projects">${projectValue}</div>
                                       <div class="status">
                                         <div class="status__head active">Active</div>
                                            <ul class="status__body">
                                               <li class="status-item" data-class="active">Active</li>
                                               <li class="status-item" data-class="inactive">Inactive</li>
                                            </ul>
                                         </div>
                                       <div class="edit">
                                          <svg class="edit__icon" viewBox="0 0 11 11" xmlns="http://www.w3.org/2000/svg">
                                              <path d="M9.03559 5.49999C8.81861 5.49999 8.64273 5.67588 8.64273 5.89285V9.82135C8.64273 10.0383 8.46684 10.2142 8.24987 10.2142H1.17856C0.961582 10.2142 0.785696 10.0383 0.785696 9.82135V1.96432C0.785696 1.74735 0.961582 1.57146 1.17856 1.57146H5.89278C6.10975 1.57146 6.28564 1.39558 6.28564 1.1786C6.28564 0.961629 6.10975 0.785767 5.89278 0.785767H1.17856C0.527657 0.785767 0 1.31342 0 1.96432V9.82135C0 10.4723 0.527657 10.9999 1.17856 10.9999H8.24989C8.90079 10.9999 9.42845 10.4723 9.42845 9.82135V5.89283C9.42845 5.67588 9.25256 5.49999 9.03559 5.49999Z" />
                                              <path d="M10.5596 0.440659C10.2776 0.158543 9.89496 8.22483e-05 9.49602 0.000128286C9.09686 -0.00102265 8.71387 0.157714 8.43258 0.440913L3.25791 5.61514C3.21498 5.65839 3.1826 5.71097 3.16324 5.76874L2.37754 8.12586C2.30897 8.33171 2.42026 8.55416 2.62612 8.62271C2.66606 8.63602 2.70788 8.64281 2.74996 8.64285C2.79213 8.64279 2.83405 8.63602 2.8741 8.62283L5.23121 7.83713C5.28911 7.8178 5.3417 7.78525 5.38482 7.74207L10.5595 2.56742C11.1468 1.98017 11.1469 1.02798 10.5596 0.440659ZM10.004 2.0123L4.89689 7.11939L3.37105 7.62891L3.879 6.10502L8.98805 0.997964C9.26892 0.717643 9.72389 0.718103 10.0042 0.998977C10.1381 1.13315 10.2136 1.31479 10.2141 1.50435C10.2146 1.69492 10.139 1.87778 10.004 2.0123Z"/>
                                          </svg>
                                        </div>
                                       <div class="delete">
                                            <svg class="delete__icon" width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                 <g clip-path="url(#clip0)">
                                                 <path d="M9.96608 2.4587L9.7232 1.73067C9.63072 1.45338 9.37215 1.26707 9.07993 1.26707H7.03866V0.602486C7.03866 0.270317 6.7686 0 6.43651 0H4.5672C4.2352 0 3.96505 0.270317 3.96505 0.602486V1.26707H1.92387C1.63157 1.26707 1.373 1.45338 1.28051 1.73067L1.03764 2.4587C0.982335 2.62445 1.01036 2.80799 1.1125 2.94974C1.21463 3.09148 1.37988 3.17616 1.55461 3.17616H1.80848L2.36724 10.0857C2.40878 10.5983 2.84384 11 3.35787 11H7.75923C8.27317 11 8.70832 10.5983 8.74978 10.0856L9.30854 3.17616H9.44911C9.62384 3.17616 9.78908 3.09148 9.89122 2.94982C9.99335 2.80807 10.0214 2.62445 9.96608 2.4587ZM4.60959 0.644531H6.39413V1.26707H4.60959V0.644531ZM8.10734 10.0336C8.09274 10.2141 7.93983 10.3555 7.75923 10.3555H3.35787C3.17727 10.3555 3.02436 10.2141 3.00975 10.0336L2.45511 3.17616H8.66191L8.10734 10.0336ZM1.69283 2.53163L1.89198 1.9346C1.89651 1.92084 1.90935 1.91161 1.92387 1.91161H9.07993C9.09445 1.91161 9.1072 1.92084 9.11182 1.9346L9.31097 2.53163H1.69283Z" fill="#B9B9BE"/>
                                                 <path d="M7.07943 10.0108C7.08514 10.0111 7.09076 10.0112 7.09647 10.0112C7.26675 10.0112 7.40909 9.87779 7.41798 9.70575L7.72061 3.89641C7.72985 3.71866 7.59322 3.56701 7.41555 3.55778C7.23738 3.54829 7.08623 3.68509 7.07691 3.86284L6.77437 9.67218C6.76514 9.84993 6.90168 10.0016 7.07943 10.0108Z" fill="#B9B9BE"/>
                                                 <path d="M3.60009 9.70651C3.60949 9.8783 3.75165 10.0112 3.9216 10.0112C3.92747 10.0112 3.93352 10.0111 3.93947 10.0107C4.11714 10.0011 4.25334 9.84918 4.24369 9.67143L3.92672 3.86209C3.91707 3.68434 3.76517 3.54813 3.58742 3.55786C3.40975 3.56752 3.27355 3.71942 3.2832 3.89717L3.60009 9.70651Z" />
                                                 <path d="M5.50537 10.0112C5.68337 10.0112 5.82764 9.86692 5.82764 9.68892V3.87958C5.82764 3.70158 5.68337 3.55731 5.50537 3.55731C5.32737 3.55731 5.18311 3.70158 5.18311 3.87958V9.68892C5.18311 9.86692 5.32737 10.0112 5.50537 10.0112Z" fill="#B9B9BE"/>
                                                 </g>
                                                 <defs>
                                                 <clipPath id="clip0">
                                                   <rect width="11" height="11" fill="white"/>
                                                 </clipPath>
                                                 </defs>
                                             </svg>
                                        </div>`;
          return li;
}

/* Remove Item in Users List */
function removeListItem():void {
    const deleteBtns = document.querySelectorAll('.delete .delete__icon');

          deleteBtns.forEach(deleteBtn => {
              deleteBtn.addEventListener('click', function():void {
                  const _this       = this,
                      deleteBtn   = _this.parentNode,
                      item        = deleteBtn.parentNode;

                  const yes = document.querySelector('#yes');
                  const no  = document.querySelector('#no');
                  const close = document.querySelector('#pop-up-window-close');
                  popUpWindow.classList.add('active');
                  popUpBlur.classList.add('active');

                  yes.addEventListener('click', function () {
                    item.remove();
                      popUpWindow.classList.remove('active');
                      popUpBlur.classList.remove('active');
                  });

                  no.addEventListener('click', function () {
                      popUpWindow.classList.remove('active');
                      popUpBlur.classList.remove('active');
                  });

                  close.addEventListener('click', function () {
                      popUpWindow.classList.remove('active');
                      popUpBlur.classList.remove('active');
                  });

              });
          });
}
removeListItem(); // Start function

/* Editing status in table */
function editItem():void {
    const editItems = document.querySelectorAll('.table-body .table-body__item .edit');

    editItems.forEach(editItem => {
        editItem.addEventListener('click', function () {
            const item = editItem.parentNode,
                  status = item.querySelector('.status');
                  status.classList.add('on-editing');
        })
    })
}

/* Choose editing status in table */
function chooseEditStatus():void {
    const editStatuses = document.querySelectorAll('.table-body .table-body__item .status .status-item');
    editStatuses.forEach(editStatus => {
        editStatus.addEventListener('click', function () {
            const currentClassName          = editStatus.getAttribute('data-class'),
                  text                      = editStatus.textContent,
                  statusBody                = editStatus.parentNode,
                  item                      = statusBody.parentNode.parentNode,
                  statusHead:HTMLElement    = item.querySelector('.status__head'),
                  status                    = item.querySelector('.status');

            status.classList.remove('on-editing');
            statusHead.innerText = `${text}`;
            statusHead.className = `status__head ${currentClassName}`;
        })
    })
}

// Start editing status in table
editItem();
// Choose editing status in table
chooseEditStatus();



/*   ----------------------**** Functions ****----------------------   */


/*   ---------------------- Listeners ----------------------   */

// Open/Close Dropdown menu
dropDownListHead.addEventListener('click', showDropDownList);
// Choose Dropdown menu Item
dropMenuItems.forEach(dropMenuItem => dropMenuItem.addEventListener('click', chooseDropDownList) );
// Start email validation on submit
usersSubmit.addEventListener('click', emailValidation);
// Remove alerts
usersEmail.addEventListener('input', removeAlert);

/*   ----------------------**** Listeners ****----------------------   */

