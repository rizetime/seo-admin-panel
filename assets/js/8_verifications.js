(function () {
    var dropzone = document.querySelector('#dropzone');
    var filename = document.querySelector('#dropzone-filename');
    var selectedFile = document.querySelector('#file-upload');
    selectedFile.onchange = function () {
        upload(selectedFile.files);
    };
    var upload = function (files) {
        var formData = new FormData(), xhr = new XMLHttpRequest(), x;
        for (x = 0; x < files.length; x = x + 1) {
            formData.append('file[]', files[x]);
        }
        xhr.onload = function () {
            var data = JSON.parse(this.responseText);
            console.log(data);
            filename.innerText = "" + data[0].name;
            var parentFileName = filename.parentNode;
            parentFileName.className = 'drop-zone-label__name loaded';
        };
        xhr.open('post', 'upload.php');
        xhr.send(formData);
    };
    dropzone.ondrop = function (e) {
        e.preventDefault();
        var _this = this;
        _this.className = 'dropzone';
        upload(e.dataTransfer.files);
        console.log('DATA TRANSFER FILES');
    };
    dropzone.addEventListener('dragover', function (e) {
        e.preventDefault();
        this.className = 'dropzone dragover';
        return false;
    });
    dropzone.addEventListener('dragleave', function () {
        this.className = 'dropzone';
        return false;
    });
}());
var dropdownListHeads = document.querySelectorAll('.dropdown-list__head'), dropMenuItems = document.querySelectorAll('.drop-menu__item');
function chooseDropDownList() {
    var _this = this, dropdownList = _this.parentNode;
    dropdownList.classList.toggle('is-open');
}
function chooseDropDownItem() {
    var _this = this, currentValue = _this.textContent, dropMenu = _this.parentNode, dropMenuBody = dropMenu.parentNode, dropdownList = dropMenuBody.parentNode, dropMenuHead = dropdownList.querySelector('.dropdown-list__head'), dropdownTitle = dropMenuHead.querySelector('.dropdown-title'), titleValue = dropdownTitle.querySelector('.dropdown-title__value'), titleInput = dropdownTitle.querySelector('input');
    titleValue.innerText = currentValue;
    titleInput.value = currentValue;
    dropdownList.classList.remove('is-open');
}
dropdownListHeads.forEach(function (dropdownListHead) { return dropdownListHead.addEventListener('click', chooseDropDownList); });
dropMenuItems.forEach(function (dropMenuItem) { return dropMenuItem.addEventListener('click', chooseDropDownItem); });
