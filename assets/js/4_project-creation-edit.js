/*   ---------------------- Constants ----------------------   */
var counterBlock = document.querySelector('#user-compaign-counter');
var minus = counterBlock.querySelector('.minus');
var plus = counterBlock.querySelector('.plus');
var userCompaignName = document.querySelector('#user-compaign-name');
/*   ----------------------**** Constants ****----------------------   */
/*   ---------------------- Functions ----------------------   */
// Increment
function increment() {
    var currentValue = counterBlock.querySelector('.form-counter__value'), initialNum = +currentValue.textContent;
    currentValue.innerText = "" + (initialNum + 1);
}
// Decrement
function decrement() {
    var currentValue = counterBlock.querySelector('.form-counter__value'), initialNum = +currentValue.textContent;
    if (initialNum !== 1)
        currentValue.innerText = "" + (initialNum - 1);
}
/*   ----------------------**** Functions ****----------------------   */
/*   ---------------------- Listeners ----------------------   */
// Increment
plus.addEventListener('click', increment);
// Decrement
minus.addEventListener('click', decrement);
/*   ----------------------**** Listeners ****----------------------   */
