/*   ---------------------- Constants ----------------------   */
const userLogout = document.querySelector('#user-logout');
const reminder   = document.querySelector('#reminder img');
/*   ----------------------**** Constants ****----------------------   */



/*   ---------------------- Functions ----------------------   */
// Open/Close Logout menu
function openUserLogoutMenu():void {
    const _this = this;
    _this.classList.toggle('is-open');
}

// Open/Close Reminder
function openReminder():void {
    const _this = this;
    const reminder = _this.parentNode;
    reminder.classList.toggle('is-open');
}

/*   ----------------------**** Functions ****----------------------   */



/*   ---------------------- Listeners ----------------------   */
// Open/Close User-logout menu
userLogout.addEventListener('click', openUserLogoutMenu);
// Open/Close Reminder
reminder.addEventListener('click', openReminder);
/*   ----------------------**** Listeners ****----------------------   */
