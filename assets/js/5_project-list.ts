/*   ---------------------- Constants ----------------------   */
const popUpWindow                         = document.querySelector('#pop-up-window'),
      popUpBlur                           = document.querySelector('#pop-up-blur');

/*   ----------------------**** Constants ****----------------------   */


/*   ---------------------- Functions ----------------------   */

/* Remove Item in Users List */
function removeListItem() {
    const deleteBtns = document.querySelectorAll('.delete .delete__icon');

    deleteBtns.forEach(deleteBtn => {
        deleteBtn.addEventListener('click', function() {
            const _this       = this,
                deleteBtn   = _this.parentNode,
                item        = deleteBtn.parentNode;

            const yes = document.querySelector('#yes');
            const no  = document.querySelector('#no');
            const close = document.querySelector('#pop-up-window-close');
            popUpWindow.classList.add('active');
            popUpBlur.classList.add('active');

            yes.addEventListener('click', function () {
                item.remove();
                popUpWindow.classList.remove('active');
                popUpBlur.classList.remove('active');
            });

            no.addEventListener('click', function () {
                popUpWindow.classList.remove('active');
                popUpBlur.classList.remove('active');
            });

            close.addEventListener('click', function () {
                popUpWindow.classList.remove('active');
                popUpBlur.classList.remove('active');
            });

        });
    });
}
removeListItem(); // Start function


function startEditing() {
    const editItems = document.querySelectorAll('.table-body .table-body__item .edit');
    editItems.forEach(editItem => {
        const editItemIcon = editItem.querySelector('.edit__icon');
        editItemIcon.addEventListener('click', function () {
            const listItem = editItem.parentNode.parentNode;

            const capacity = listItem.querySelector('.capacity');
            const capacityName:HTMLElement = capacity.querySelector('.capacity__name');
            const capacityValue:HTMLInputElement = capacity.querySelector('.capacity__value');

            const exclusiveLinks = listItem.querySelector('.exclusive-links');
            const exclusiveLinksName:HTMLElement = listItem.querySelector('.exclusive-links__name');
            const exclusiveLinksValue:HTMLInputElement = listItem.querySelector('.exclusive-links__value');

            const linkToProject = listItem.querySelector('.link-to-project');
            const linkToProjectName:HTMLElement = listItem.querySelector('.link-to-project__name');
            const linkToProjectValue:HTMLInputElement = listItem.querySelector('.link-to-project__value');

            const link = listItem.querySelector('.link');
            const editItemIconChecked = editItemIcon.nextElementSibling;
            link.classList.add('on-editing');
            capacity.classList.add('on-editing');
            exclusiveLinks.classList.add('on-editing');
            linkToProject.classList.add('on-editing');

            const linkName:HTMLElement = link.querySelector('.link__name');
            const linkHref = link.querySelector('.link__href');
            const linkNameValue:HTMLInputElement = link.querySelector('.link__name--value');
            const linkNameValueHref:HTMLInputElement = link.querySelector('.link__name--value-href');

            editItemIcon.classList.add('hidden');
            editItemIconChecked.classList.remove('hidden');

            editItemIconChecked.addEventListener('click', function () {
                linkName.innerText = `${linkNameValue.value}`;
                linkHref.setAttribute('href', `${linkNameValueHref.value}`);
                linkHref.setAttribute('title', `${linkNameValueHref.value}`);
                link.classList.remove('on-editing');

                capacityName.innerText = `${capacityValue.value}`;
                capacity.classList.remove('on-editing');

                exclusiveLinksName.innerText = `${exclusiveLinksValue.value}`;
                exclusiveLinks.classList.remove('on-editing');

                linkToProjectName.innerText = `${linkToProjectValue.value}`;
                linkToProjectName.setAttribute('href', `${linkToProjectValue.value}`);
                linkToProject.classList.remove('on-editing');

                editItemIcon.classList.remove('hidden');
                editItemIconChecked.classList.add('hidden');
            });
        });
    })
}


startEditing();

/*   ----------------------**** Functions ****----------------------   */


