/*   ---------------------- Constants ----------------------   */
const postingProjectValue:HTMLInputElement      = document.querySelector('#posting-project-value'),
        projectCapacityValue:HTMLInputElement   = document.querySelector('#project-capacity-value'),
        fileInputValue:HTMLInputElement         = document.querySelector('#file-input-value'),
        postingCampaignName:HTMLInputElement    = document.querySelector('#posting-campaign-name'),
        postingMaxLink:HTMLInputElement         = document.querySelector('#posting-max-link'),
        dropdownListHeads                       = document.querySelectorAll('.dropdown-list__head'),
        dropMenuItems                           = document.querySelectorAll('.drop-menu__item'),
        postingSubmit                           = document.querySelector('#posting-submit'),
        loader                                  = document.querySelector('.loader-circle');
/*   ----------------------**** Constants ****----------------------   */


/*   ---------------------- Functions ----------------------   */

function chooseDropDownList():void {
    const _this = this,
        dropdownList = _this.parentNode;
    dropdownList.classList.toggle('is-open');
}

function chooseDropDownItem():void {
    const _this = this,
        currentValue = _this.textContent,
        dropMenu = _this.parentNode,
        dropMenuBody = dropMenu.parentNode,
        dropdownList = dropMenuBody.parentNode,
        dropMenuHead = dropdownList.querySelector('.dropdown-list__head'),
        dropdownTitle = dropMenuHead.querySelector('.dropdown-title'),
        titleValue =  dropdownTitle.querySelector('.dropdown-title__value'),
        titleInput =  dropdownTitle.querySelector('input');

    titleValue.innerText = currentValue;
    titleInput.value = currentValue;

    dropdownList.classList.remove('is-open');
}

/* Remove Alert */
function removeAlert():void {
    const _this = this;
    _this.classList.remove('invalid');
}

/* Request emulating */
function startRequest():void {
    setTimeout(()=>{
        postingCampaignName.classList.remove('confirmed');
        postingMaxLink.classList.remove('confirmed');
        postingSubmit.removeAttribute('disabled');
        loader.classList.remove('active');

        let data = {
            "project": postingProjectValue.value,
            "name": postingCampaignName.value,
            "capacity": projectCapacityValue.value,
            "links": postingMaxLink.value,
            "file": fileInputValue.value,
        }

        console.log(data);

        setTimeout(()=>{
            postingCampaignName.value = '';
            postingMaxLink.value = '';
        }, 200)
    },500);
}

function startValidation():void {
    const standardInputs = document.querySelectorAll('.input-standard');
    standardInputs.forEach((standardInput:HTMLInputElement) => {
        standardInput.addEventListener('input', removeAlert);
        if (standardInput.value !== "") {
            standardInput.classList.add('confirmed');
            postingSubmit.setAttribute('disabled', 'disabled');
            loader.classList.add('active');
            startRequest();
        } else {
            standardInput.classList.add('invalid');
        }
    })
}

/*   ----------------------**** Functions ****----------------------   */


/*   ---------------------- Listeners ----------------------   */

dropdownListHeads.forEach(dropdownListHead => dropdownListHead.addEventListener('click', chooseDropDownList) );

dropMenuItems.forEach(dropMenuItem => dropMenuItem.addEventListener('click', chooseDropDownItem) );

postingSubmit.addEventListener('click', startValidation);

/*   ----------------------**** Listeners ****----------------------   */



