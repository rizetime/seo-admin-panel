(function () {
    let dropzone: HTMLElement = document.querySelector('#dropzone');
    let filename: HTMLElement = document.querySelector('#dropzone-filename');
    const selectedFile: HTMLInputElement = document.querySelector('#file-upload');

    selectedFile.onchange = function () {
        upload(selectedFile.files);
    }


    let upload = function (files) {
        let formData = new FormData(),
            xhr = new XMLHttpRequest(),
            x;

        for (x = 0; x < files.length; x = x + 1) {
            formData.append('file[]', files[x]);
        }

        xhr.onload = function () {
            let data = JSON.parse(this.responseText);
            console.log(data);
            filename.innerText = `${data[0].name}`;
            const parentFileName: any = filename.parentNode;
            parentFileName.className = 'drop-zone-label__name loaded';

        }

        xhr.open('post', 'upload.php');
        xhr.send(formData);
    }

    dropzone.ondrop = function (e: any): void {
        e.preventDefault();
        const _this: any = this;
        _this.className = 'dropzone';
        upload(e.dataTransfer.files);
        console.log('DATA TRANSFER FILES');
    }

    dropzone.addEventListener('dragover', function (e) {
        e.preventDefault();
        this.className = 'dropzone dragover';
        return false;
    });

    dropzone.addEventListener('dragleave', function () {
        this.className = 'dropzone';
        return false;
    });
}());


const dropdownListHeads = document.querySelectorAll('.dropdown-list__head'),
    dropMenuItems = document.querySelectorAll('.drop-menu__item');

function chooseDropDownList() {
    const _this = this,
        dropdownList = _this.parentNode;
    dropdownList.classList.toggle('is-open');
}

function chooseDropDownItem() {
    const _this = this,
        currentValue = _this.textContent,
        dropMenu = _this.parentNode,
        dropMenuBody = dropMenu.parentNode,
        dropdownList = dropMenuBody.parentNode,
        dropMenuHead = dropdownList.querySelector('.dropdown-list__head'),
        dropdownTitle = dropMenuHead.querySelector('.dropdown-title'),
        titleValue = dropdownTitle.querySelector('.dropdown-title__value'),
        titleInput = dropdownTitle.querySelector('input');

    titleValue.innerText = currentValue;
    titleInput.value = currentValue;

    dropdownList.classList.remove('is-open');
}

dropdownListHeads.forEach(dropdownListHead => dropdownListHead.addEventListener('click', chooseDropDownList));

dropMenuItems.forEach(dropMenuItem => dropMenuItem.addEventListener('click', chooseDropDownItem));

