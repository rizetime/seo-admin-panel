/*   ---------------------- Constants ----------------------   */
const standardInputs = document.querySelectorAll('.input-standard');
/*   ----------------------**** Constants ****----------------------   */



/*   ---------------------- Functions ----------------------   */

/* Field on Focus */
function fieldOnFocus() { // the script adds a heading at the top with ::focus
    const _this = this;
    const title = _this.parentNode.querySelector('.label-title');
    _this.classList.add('on-focus');
    title.classList.add('on-focus');
    title.classList.add('active');
}

/* Field on Blur */
function fieldOnBlur() { // the script remove a heading at the top with ::blur
    const _this = this;
    const title = _this.parentNode.querySelector('.label-title');
    title.classList.remove('active');
    if (_this.value === "") {
        _this.classList.remove('on-focus');
        title.classList.remove('on-focus');
    }
}

/*   ----------------------**** Functions ****----------------------   */


/*   ---------------------- Listeners ----------------------   */
standardInputs.forEach(standardInput => {
    standardInput.addEventListener('focus', fieldOnFocus);
    standardInput.addEventListener('blur', fieldOnBlur);
});
/*   ----------------------**** Listeners ****----------------------   */
