/*   ---------------------- Constants ----------------------   */
var standardInputs = document.querySelectorAll('.input-standard');
/*   ----------------------**** Constants ****----------------------   */
/*   ---------------------- Functions ----------------------   */
/* Field on Focus */
function fieldOnFocus() {
    var _this = this;
    var title = _this.parentNode.querySelector('.label-title');
    _this.classList.add('on-focus');
    title.classList.add('on-focus');
    title.classList.add('active');
}
/* Field on Blur */
function fieldOnBlur() {
    var _this = this;
    var title = _this.parentNode.querySelector('.label-title');
    title.classList.remove('active');
    if (_this.value === "") {
        _this.classList.remove('on-focus');
        title.classList.remove('on-focus');
    }
}
/*   ----------------------**** Functions ****----------------------   */
/*   ---------------------- Listeners ----------------------   */
standardInputs.forEach(function (standardInput) {
    standardInput.addEventListener('focus', fieldOnFocus);
    standardInput.addEventListener('blur', fieldOnBlur);
});
/*   ----------------------**** Listeners ****----------------------   */
