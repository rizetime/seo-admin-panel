/*   ---------------------- Constants ----------------------   */
const counterBlock                      = document.querySelector('#user-compaign-counter');
const minus                             = counterBlock.querySelector('.minus');
const plus                              = counterBlock.querySelector('.plus');
const userCompaignName:HTMLInputElement = document.querySelector('#user-compaign-name');
/*   ----------------------**** Constants ****----------------------   */


/*   ---------------------- Functions ----------------------   */

// Increment
function increment():void {
    const currentValue:HTMLElement  = counterBlock.querySelector('.form-counter__value'),
          initialNum:number         = +currentValue.textContent;
          currentValue.innerText    = `${initialNum + 1}`;
}

// Decrement
function decrement():void {
    const currentValue:HTMLElement = counterBlock.querySelector('.form-counter__value'),
          initialNum:number        = +currentValue.textContent;

    if (initialNum !== 1) currentValue.innerText = `${initialNum - 1}`;
}

/*   ----------------------**** Functions ****----------------------   */



/*   ---------------------- Listeners ----------------------   */
// Increment
plus.addEventListener('click', increment);
// Decrement
minus.addEventListener('click', decrement);


/*   ----------------------**** Listeners ****----------------------   */
