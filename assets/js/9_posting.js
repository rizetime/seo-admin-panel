/*   ---------------------- Constants ----------------------   */
var postingProjectValue = document.querySelector('#posting-project-value'), projectCapacityValue = document.querySelector('#project-capacity-value'), fileInputValue = document.querySelector('#file-input-value'), postingCampaignName = document.querySelector('#posting-campaign-name'), postingMaxLink = document.querySelector('#posting-max-link'), dropdownListHeads = document.querySelectorAll('.dropdown-list__head'), dropMenuItems = document.querySelectorAll('.drop-menu__item'), postingSubmit = document.querySelector('#posting-submit'), loader = document.querySelector('.loader-circle');
/*   ----------------------**** Constants ****----------------------   */
/*   ---------------------- Functions ----------------------   */
function chooseDropDownList() {
    var _this = this, dropdownList = _this.parentNode;
    dropdownList.classList.toggle('is-open');
}
function chooseDropDownItem() {
    var _this = this, currentValue = _this.textContent, dropMenu = _this.parentNode, dropMenuBody = dropMenu.parentNode, dropdownList = dropMenuBody.parentNode, dropMenuHead = dropdownList.querySelector('.dropdown-list__head'), dropdownTitle = dropMenuHead.querySelector('.dropdown-title'), titleValue = dropdownTitle.querySelector('.dropdown-title__value'), titleInput = dropdownTitle.querySelector('input');
    titleValue.innerText = currentValue;
    titleInput.value = currentValue;
    dropdownList.classList.remove('is-open');
}
/* Remove Alert */
function removeAlert() {
    var _this = this;
    _this.classList.remove('invalid');
}
/* Request emulating */
function startRequest() {
    setTimeout(function () {
        postingCampaignName.classList.remove('confirmed');
        postingMaxLink.classList.remove('confirmed');
        postingSubmit.removeAttribute('disabled');
        loader.classList.remove('active');
        var data = {
            "project": postingProjectValue.value,
            "name": postingCampaignName.value,
            "capacity": projectCapacityValue.value,
            "links": postingMaxLink.value,
            "file": fileInputValue.value
        };
        console.log(data);
        setTimeout(function () {
            postingCampaignName.value = '';
            postingMaxLink.value = '';
        }, 200);
    }, 500);
}
function startValidation() {
    var standardInputs = document.querySelectorAll('.input-standard');
    standardInputs.forEach(function (standardInput) {
        standardInput.addEventListener('input', removeAlert);
        if (standardInput.value !== "") {
            standardInput.classList.add('confirmed');
            postingSubmit.setAttribute('disabled', 'disabled');
            loader.classList.add('active');
            startRequest();
        }
        else {
            standardInput.classList.add('invalid');
        }
    });
}
/*   ----------------------**** Functions ****----------------------   */
/*   ---------------------- Listeners ----------------------   */
dropdownListHeads.forEach(function (dropdownListHead) { return dropdownListHead.addEventListener('click', chooseDropDownList); });
dropMenuItems.forEach(function (dropMenuItem) { return dropMenuItem.addEventListener('click', chooseDropDownItem); });
postingSubmit.addEventListener('click', startValidation);
/*   ----------------------**** Listeners ****----------------------   */
