/*   ---------------------- Constants ----------------------   */
var popUpWindow = document.querySelector('#pop-up-window'), popUpBlur = document.querySelector('#pop-up-blur');
/*   ----------------------**** Constants ****----------------------   */
/*   ---------------------- Functions ----------------------   */
/* Remove Item in Users List */
function removeListItem() {
    var deleteBtns = document.querySelectorAll('.delete .delete__icon');
    deleteBtns.forEach(function (deleteBtn) {
        deleteBtn.addEventListener('click', function () {
            var _this = this, deleteBtn = _this.parentNode, item = deleteBtn.parentNode;
            var yes = document.querySelector('#yes');
            var no = document.querySelector('#no');
            var close = document.querySelector('#pop-up-window-close');
            popUpWindow.classList.add('active');
            popUpBlur.classList.add('active');
            yes.addEventListener('click', function () {
                item.remove();
                popUpWindow.classList.remove('active');
                popUpBlur.classList.remove('active');
            });
            no.addEventListener('click', function () {
                popUpWindow.classList.remove('active');
                popUpBlur.classList.remove('active');
            });
            close.addEventListener('click', function () {
                popUpWindow.classList.remove('active');
                popUpBlur.classList.remove('active');
            });
        });
    });
}
removeListItem(); // Start function
function startEditing() {
    var editItems = document.querySelectorAll('.table-body .table-body__item .edit');
    editItems.forEach(function (editItem) {
        var editItemIcon = editItem.querySelector('.edit__icon');
        editItemIcon.addEventListener('click', function () {
            var listItem = editItem.parentNode.parentNode;
            var capacity = listItem.querySelector('.capacity');
            var capacityName = capacity.querySelector('.capacity__name');
            var capacityValue = capacity.querySelector('.capacity__value');
            var exclusiveLinks = listItem.querySelector('.exclusive-links');
            var exclusiveLinksName = listItem.querySelector('.exclusive-links__name');
            var exclusiveLinksValue = listItem.querySelector('.exclusive-links__value');
            var linkToProject = listItem.querySelector('.link-to-project');
            var linkToProjectName = listItem.querySelector('.link-to-project__name');
            var linkToProjectValue = listItem.querySelector('.link-to-project__value');
            var link = listItem.querySelector('.link');
            var editItemIconChecked = editItemIcon.nextElementSibling;
            link.classList.add('on-editing');
            capacity.classList.add('on-editing');
            exclusiveLinks.classList.add('on-editing');
            linkToProject.classList.add('on-editing');
            var linkName = link.querySelector('.link__name');
            var linkHref = link.querySelector('.link__href');
            var linkNameValue = link.querySelector('.link__name--value');
            var linkNameValueHref = link.querySelector('.link__name--value-href');
            editItemIcon.classList.add('hidden');
            editItemIconChecked.classList.remove('hidden');
            editItemIconChecked.addEventListener('click', function () {
                linkName.innerText = "" + linkNameValue.value;
                linkHref.setAttribute('href', "" + linkNameValueHref.value);
                linkHref.setAttribute('title', "" + linkNameValueHref.value);
                link.classList.remove('on-editing');
                capacityName.innerText = "" + capacityValue.value;
                capacity.classList.remove('on-editing');
                exclusiveLinksName.innerText = "" + exclusiveLinksValue.value;
                exclusiveLinks.classList.remove('on-editing');
                linkToProjectName.innerText = "" + linkToProjectValue.value;
                linkToProjectName.setAttribute('href', "" + linkToProjectValue.value);
                linkToProject.classList.remove('on-editing');
                editItemIcon.classList.remove('hidden');
                editItemIconChecked.classList.add('hidden');
            });
        });
    });
}
startEditing();
/*   ----------------------**** Functions ****----------------------   */
