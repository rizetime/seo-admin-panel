/*   ---------------------- Constants ----------------------   */
const userAuthorizationSubmit:HTMLButtonElement  = document.querySelector('#user-authorization-submit'),
      authorizationBottom                        = document.querySelector('#authorization-bottom'),
      loader                                     = document.querySelector('.loader-circle'),
      userPass:HTMLInputElement                  = document.querySelector('#user-pass'),
      userLogin:HTMLInputElement                 = document.querySelector('#user-login'),
      showPassBtn                                = document.querySelector('#show-pass');
/*   ----------------------**** Constants ****----------------------   */


/*   ---------------------- Functions ----------------------   */

/* Show/Hide Password */
let isPassHide: boolean = true; // initial state
function showPassword():void {
    const userPass:HTMLInputElement = document.querySelector('#user-pass');
    isPassHide                      = !isPassHide;
    userPass.setAttribute('type', isPassHide ? 'password' : 'text');

    const _this = this;
    _this.classList.toggle('active');
}

/* Request emulating */
function startRequest():void {
    setTimeout(()=>{
        userPass.value  = '';
        userLogin.value = '';
        userPass.classList.remove('confirmed');
        userLogin.classList.remove('confirmed');
        userAuthorizationSubmit.removeAttribute('disabled');
        loader.classList.remove('active');
    },500);
}

/* Authorization */
function authorizationValidation():void {
    const login: boolean = true;
    const pass: boolean  = true;

    if (login && pass) { // If login and password correct
        userPass.classList.add('confirmed');
        userLogin.classList.add('confirmed');
        userAuthorizationSubmit.setAttribute('disabled', 'disabled');
        loader.classList.add('active');
        startRequest();
    } else { // If login or password incorrect
        userPass.classList.add('invalid');
        userLogin.classList.add('invalid');
        authorizationBottom.classList.add('invalid');
    }
}

/* Remove Alert */
function removeAlert():void { // Script removes all "alert" borders
    authorizationBottom.classList.remove('invalid');
    userLogin.classList.remove('invalid');
    userPass.classList.remove('invalid');
}

/*   ----------------------**** Functions ****----------------------   */



/*   ---------------------- Listeners ----------------------   */

// Show/Hide password
showPassBtn.addEventListener('click', showPassword);
// Start authorization
userAuthorizationSubmit.addEventListener('click', authorizationValidation);
// Remove alerts
userPass.addEventListener('input', removeAlert);
userLogin.addEventListener('input', removeAlert);

/*   ----------------------**** Listeners ****----------------------   */
